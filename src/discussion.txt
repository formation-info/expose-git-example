{{textinfo 
|edition = Published as the second appendix to the book ''Methodus inveniendi lineas curvas...'' (1744) by Leonhard Euler
|source = 
|progress = Text completed and proofread by contributor [[Image:75%.svg]]
|notes = Not checked
|contributors = [[User:WillowW|WillowW]]
|proofreaders = }}

Hi, I undertook this translation of Euler's appendix because I needed it for a planned series of articles about the various types of the "action principle" in physics and this history of variational principles in physics.  It was fun, especially seeing how the science of mechanics developed. 

The manuscript from which I worked may be found 
[http://math.dartmouth.edu/~euler/docs/originals/E065h here].  The original has also been added to the [[:la:Methodus inveniendi|Latin Wikisource]].

This translation of Euler's ''Methodus inveniendi -- Additamentum II'' is hereby released into the public domain.  Use it well. [[User:WillowW|WillowW]] 11:37, 25 June 2006 (UTC)


'''Historical notes'''

This appendix describes Euler's independent invention of '''Maupertuis' principle''', which states that the true trajectory followed by a mechanical system between two specified endpoints is an extremum of the integral of momentum over distance travelled, <math>\int p dq</math> in modern notation.  Maupertuis' principle is ''not'' the same as '''Hamilton's principle'''; the former considers only the path through ''space'' and requires the conservation of energy, whereas the latter considers the path through ''space and time'' and has no special requirements.  Euler lays the groundwork here for Hamilton's principle by noting that the integral of momentum over distance could be converted into an integral of kinetic energy over time (paragraph 2); however, his main purpose in doing so was to make his minimum principle acceptable to the followers of both schools of mechanics (that of Newton and that of Leibniz), who were having a bitter conflict long after both physicists were dead.

Maupertuis and Euler conceived this "principle of least action" independently of one another, but Maupertuis has priority, since he submitted it for publication a few months prior to Euler (1744).  Euler himself gave Maupertuis credit for this principle in his later articles and defended his priority vigourously when it was challenged in 1751 by Samuel König.  However, neither Maupertuis nor Euler got the principle quite right; neither recognized the requirement of energy conservation explicitly, nor the possibility of other types of extrema (maxima or inflection) of the integral.  In contrast to Maupertuis, however, Euler recognized that his principle pertains only when the speed is a function of position alone and presents it here as a hypothesis worthy of further investigation.

In paragraph 11, Euler lays the groundwork for Lagrange's ''generalized coordinates'' by noting that his principle can be expressed in different coordinates and that this is a useful trick for solving a wider variety of problems than can be solved merely in Cartesian coordinates.  Euler illustrates this by solving the "central force" problem in polar coordinates.  In paragraph 16, Euler also states without proof that the principle can be applied to ''systems'' of particles, rather than merely to a single particle (as done in the remainder of the article and also by Maupertuis).    


'''Translation status:''' Complete, but minor touchups might still be needed.  Three obscure readings could use some clarification from a better Latin/physics scholar than me; see the translation notes below.  Thanks for your help! :D

'''Notes on the Translation'''   

I have a few (minor) changes in Euler's presentation to make the article more intuitive for modern readers:

* Consistent with modern custom, I used <math>v</math> and <math>v^{2}</math> for the speed and squared speed, respectively.  In the original article, Euler uses <math>\sqrt{v}</math> and <math>v</math> for the same quantities, although he uses <math>v</math> and <math>v^{2}</math> in other articles.  Euler's <math>v</math> might stand for ''vis viva'' (kinetic energy).  As an aside, "speed/velocity" in Latin (''celeritas'') starts with a "c", not a "v".

* Consistent with modern custom, I took <math>x</math> and <math>y</math> as the horizontal and vertical coordinates, respectively.  In the original article, Euler chose the reverse.

* I used the variable <math>t</math> ''only'' for time, the variable <math>r</math> ''only'' for the radius to the origin of the coordinate system and <math>R</math> for the instantaneous radius of curvature of the trajectory.  In the original article, Euler used <math>t</math> for the first two variables (albeit in well-distinguished settings) and <math>r</math> for the last variable.

* When Euler says "force" (''vis''), he seems to mean a quantity that is ''half'' of our modern use of the word "force".  Therefore, I have introduced a factor of two into the force equations.

* I have indicated force components by an ''F'' with a subscript, e.g., the radial force <math>F_{r}</math>.  In the original article, Euler uses various capital letters (<math>X, Y, T</math> and <math>V</math>) which may be confused with constants of integration.  It should be remembered that Euler was writing long before the development of vectors.

* Consistent with their notation and usage in the article, I translated ''spatiolus <math>ds</math>'', ''tempusculus <math>dt</math>'' and ''arculus'' as "infinitesimal distance <math>ds</math>", "infinitesimal time <math>dt</math>" and "infinitesimal arc".

* I translated ''corpus projectum'' as "particle" or "moving particle", since Euler clearly is discussing the trajectories of masses moving under the influence of generic external forces, and not merely the flight of cannonballs in a uniform gravitational field.  Moreover, the rotational degrees of freedom of the mass are not considered.

* I did not include a translation of the phrase ''debita altitudine'' ("resulting from its height") in the first line of paragraph 2.  Read in the context of the whole article, Euler is likely trying to specify that the velocity must be a function only of position, but the phrase seems unnecessary and distracts from the main exposition.

* In paragraph 12, Euler does not use a unit circle, but rather one of generic radius <math>c</math>.  To simplify the presentation, I have set <math>c=1</math>, with no loss of generality.

* The last sentence of paragraph 5 is a little obscure to me; I think I've translated it OK, but it may need correcting.  Suggestions are welcome!

* The conversion of B and C into their negatives in paragraph 8 is also obscure to me.  Can anyone help me out? :)

* The Latin sentence beginning "Because of their inertia" near the end of paragraph 16 is also obscure to me.  Suggestions here are also welcome!

In general, I tried to "breathe life" into the translation, i.e., not translate it word-by-word, but capture Euler's  ''meaning'' in modern English.  It wasn't hard, since Euler is quite modern in his thinking, albeit with a few twists.  I like his use of the instantaneous radius of curvature of the trajectory, which is rarely seen in physics textbooks, being found mainly in math books (differential geometry).  I also like his liberal usage of the Bernoulli <math>p</math> variable and, more generally, implicit differentiation.

