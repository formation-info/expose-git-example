### Comment collaborer sur ce manuscrit

##### 1. Récupérer une première version en clonant le dépôt distant

Sur la machine locale :

	git clone git@gitlab.math.unistra.fr:euler/appendix-II.git

Le clone ne contient que :

- le fichier LaTeX
- les fichiers images
- le README file

**Mais pas le pdf !**

##### 2. Ajouter le clone du dépôt dans GitHub Desktop

##### 3. Compiler le document LaTeX

	pdflatex methodus-inveniendi-addII.tex

##### 4. Editer les modifications

##### 5. Faire un *commit* des modifications dans GitHub Desktop

##### 6. Synchroniser avec le dépôt gitlab grâce à la commande *Sync*


### Notes additionnelles

##### Pour convertir le fichier source de la [page wikipedia](https://en.wikisource.org/wiki/Methodus_inveniendi/Additamentum_II) en fichier latex

    pandoc methodus-inveniendi-addII.txt -f mediawiki -t latex --standalone -o methodus-inveniendi-addII.tex
    
##### Le Scan de l'ouvrage original se trouve [ici](https://math.dartmouth.edu/~euler/docs/originals/E065h)