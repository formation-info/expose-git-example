\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
  \newcommand{\euro}{€}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color} % color is loaded by hyperref
\hypersetup{unicode=true,
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}

\usepackage{caption}
\usepackage{array}

\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}

\title{Methodus Inveniendi - Additamentum II}
\author{Leonhard Euler}
\date{1744}

% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

\begin{document}

\maketitle

\begin{figure}[htbp]
\centering
\includegraphics[width=0.49\textwidth]{Fig/Methodus_inveniendi_-_Leonhard_Euler_-_1744.jpg}
\includegraphics[width=0.49\textwidth]{Fig/p309.pdf}
\end{figure}


\subsection{Source}

This document has been converted into \LaTeX~from the Wikisource web page \url{https://en.wikisource.org/wiki/Methodus_inveniendi/Additamentum_II} (4 September 2015) using pandoc.

\begin{tabular}{l p{8cm}}
\textbf{Edition:} & Published as the second appendix to the book ''Methodus inveniendi lineas curvas...'' (1744) by Leonhard Euler \\
\textbf{Contributor:} & \href{https://en.wikisource.org/wiki/User:WillowW}{WillowW} \\
\textbf{Level of progress:} & Text completed and proofread by contributor \\
\textbf{Notes:} & Not checked
\end{tabular}

\clearpage

\section{Appendix 2}

\emph{Concerning the motion of particles in a non-resistant medium,
determined by a method of maxima and minima}

\textbf{1.} Since all natural phenomena obey a certain maximum or
minimum law; there is no doubt that \emph{some} property must be
maximized or minimized in the trajectories of particles acted upon by
external forces. However, it does not seem easy to determine
\emph{which} property is minimized from metaphysical principles known
\emph{a priori}. Yet if the trajectories can be determined by a direct
method, the property being minimized or maximized by these trajectories
can be determined, provided that sufficient care is taken. After
considering the effects of external forces and the movements they
generate, it seems most consistent with experience to assert that the
\textbf{integrated momentum} (i.e., the sum of all momenta contained in
the particle's movement) is the minimized quantity. This assertion is
not sufficiently proven at present; however, if I can show it to be
connected with some truth known \emph{a priori}, it will carry such
weight as to utterly vanquish every conceivable doubt. If indeed it's
truth can be verified, this assertion will make it easier to investigate
the deepest laws of Nature and their final causes, and also easier to
identify a firmer rationale for this assertion.

\textbf{2.} Let the mass of a moving particle be \(M\), and let its
speed be \(v\) while being moved over an infinitesimal distance
\emph{ds}. The particle will have a momentum \(M v\) that, when
multiplied by the distance \emph{ds}, gives \(M v ds\), the momentum of
the particle integrated over the distance \emph{ds}. Now \textbf{I
assert} that the true trajectory of the moving particle is the
trajectory to be described (from among all possible trajectories
connecting the same endpoints) that minimizes \(\int M v ds\) or (since
\emph{M} is constant) \(\int v ds\). Since the speed \(v\) resulting
from the external forces can be calculated \emph{a posteriori} from the
trajectory itself, a method of maxima and minima should suffice to
determine the trajectory \emph{a priori}. The minimized integral can be
expressed in terms of the momentum (as above), but also in terms of the
kinetic energy. For, given an infinitesimal time \(dt\) during which the
element \(ds\) is traversed, we have \(ds = v dt\). Hence,
\(\int M v ds = \int M v^{2} dt\) is minimized, i.e., the true
trajectory of a moving particle minimizes the integral over time of its
instantaneous kinetic energies. Thus, this minimum principle should
appeal both to those who favor momentum for mechanics calculations and
to those who favor kinetic energy.

\textbf{3.} For our first example, consider a moving particle free of
external forces, which has a constant speed, denoted \(b\). By our
principle, such a particle describes a trajectory that minimizes
\(\int b ds\) or \(\int ds = s\). Hence, the true path of a free
particle has the minimum length of all paths connecting the same
endpoints; this path is a straight line, just as the first principles of
Mechanics postulate. I do not present this example as evidence for the
general principle, since the integral of \emph{any} function of the
constant speed \(v=b\) would, upon minimization, produce a straight
line. I begin with this simple case merely to illustrate the reasoning.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{Fig/Euler_Methodus_inveniendi_Additamentum_II_Fig26.png}
\caption*{Figure 26}
\end{figure}

\textbf{4.} Let us proceed to the case of uniform gravity or, more
generally, to the case in which a moving particle is acted upon by a
downwards force of constant acceleration \(g\). Let the trajectory of
the particle under these conditions be AM (Figure 26), let Mm represent
the infinitesimal distance \(ds\), and let \(x\) and \(y\) represent the
vertical coordinate along AP and the horizontal coordinate along the
perpendicular axis PM, respectively. The external force produces an
acceleration described by \(dv^{2} = 2 g dy\), which integrates to
\(v^{2} = v_{0}^{2} + 2 g y\). Hence, we seek the trajectory that
minimizes the integral \(\int ds \sqrt{v_{0}^{2} + 2 g y}\). Defining
\(p \equiv \frac{dx}{dy}\), we have \(ds = dy \sqrt{1 + p^{2}}\); in
other words, we seek the trajectory that minimizes
\(\int dy \sqrt{v_{0}^{2} + 2 g y} \, \sqrt{1 + p^{2}}\). Comparing this
expression with our general formula \(\int Z dy\), we see that
\(Z = \sqrt{v_{0}^{2} + 2 g y} \, \sqrt{1 + p^{2}}\) and, given that
\(dZ \equiv M dx + N dy + P dp\), we obtain \(M=0\) and
\(P = \frac{p\sqrt{v_{0}^{2} + 2 g y}}{\sqrt{1 + p^{2}}}\). Minimization
requires that \(M dy = dP\) and, since \(M=0\) in this case, \(dP = 0\)
and, thus, \(P = \sqrt{C}\), where \(C\) is a constant. Hence, we have
the differential equation
\begin{equation}
\sqrt{C} = \frac{p\sqrt{v_{0}^{2} + 2 g y}}{\sqrt{1 + p^{2}}} = \frac{dx\sqrt{v_{0}^{2} + 2 g y}}{ds}
\end{equation}

This may be rearranged to
\(C dx^{2} + C dy^{2} = dx^{2} \left( v_{0}^{2} + 2 g y \right)\) and
separated \(dx = \frac{dy \sqrt{C}}{\sqrt{v_{0}^{2} - C + 2 g y}}\).
Integration yields the trajectory solution
\begin{equation}
x = \frac{1}{g} \sqrt{C \left( v_{0}^{2} - C + 2 g y \right)}
\end{equation}

\textbf{5.} This solution is obviously a parabola, but we should
consider more carefully whether it agrees with experience. The initial
tangent to the trajectory is clearly horizontal, i.e., \(dy=0\) at the
point where \(v_{0}^{2} - C + g y = 0\). Since the origin (0,0) of the
coordinate system may be chosen at will, we set it at that tangent
point, (i.e., at the highest point of the trajectory), which is
equivalent to setting \(C = v_{0}^{2}\). In this coordinate frame, the
trajectory solution is \(x = v_{0} \sqrt{\frac{2y}{g}}\), which is a
parabola. Moreover, since the initial speed is evidently \(v_{0}\), the
height CA from which the falling body acquires the same speed from the
same force \(g\) is \(\frac{v_{0}^{2}}{2g}\), just as predicted by the
direct methods of mechanics.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{Fig/Euler_Methodus_inveniendi_Additamentum_II_Fig27.png}
\caption*{Figure 27}
\end{figure}

\textbf{6.} Now let the particle be acted upon by a downwards vertical
force that depends on the height CP (Figure 27). We set \(y\) equal to
the vertical coordinate along CP, and let \(F_{y}(y)\) be a downwards
vertical force that is a function of \(y\). Similarly, let the
horizontal coordinate along PM be denoted as \(x\) and let Mm represent
the infinitesimal distance \(ds\). Defining \(p \equiv \frac{dx}{dy}\),
we have \(dv^{2} = 2 F_{y} \, dy\) and, thus,
\(v^{2} = v_{0}^{2} + \int 2 F_{y} \, dy\). Hence, we seek the
trajectory that minimizes the integral
\(\int dy \sqrt{v_{0}^{2} + \int 2 F_{y} \, dy} \, \sqrt{1 + p^{2}}\).
By arguments analogous to those used in paragraph 4, we obtain the
trajectory equation
\begin{equation}
\sqrt{C} = \frac{p\sqrt{v_{0}^{2} + \int 2 F_{y} \, dy}}{\sqrt{1 + p^{2}}}
\end{equation}
or, equivalently,
\(p = \frac{\sqrt{C}}{\sqrt{v_{0}^{2} - C + \int 2 F_{y} \, dy}}\),
which may be simplified to
\(x = \int \frac{dy\sqrt{C}}{\sqrt{v_{0}^{2} - C + \int 2 F_{y} \, dy}}\).
The tangent will be horizontal whenever
\(\int 2 F_{y} \, dy = C - v_{0}^{2}\). These results agree with the
trajectories predicted by direct methods of mechanics.

\textbf{7.} Now let the body be acted upon by two external forces,
denoted as \(F_{x}\) in the horizontal direction MP and \(F_{y}\) in the
vertical direction MQ (Figure 27). Let \(F_{x}(x)\) be a function of the
horizontal distance PM = \(x\) and \(F_{y}(y)\) be a function of the
vertical height MQ = CP = \(y\). Defining \(p \equiv \frac{dx}{dy}\) as
before, we have \(dv^{2} = -2 F_{x} dx - 2 F_{y} dy\), which integrates
to \(v^{2} = v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy\). Hence, we
seek the trajectory that minimizes the integral
\(\int dy \sqrt{1 + p^{2}} \sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}\).
Differentiating the integrand
\(\sqrt{1 + p^{2}} \sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}\)
yields

\[\frac{-F_{x} dx \sqrt{1 + p^{2}}}{\sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}} - 
\frac{F_{y} dy \sqrt{1 + p^{2}}}{\sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}} + 
\frac{p dp \sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}{\sqrt{1 + p^{2}}}\]

Setting
\(M=\frac{-F_{x} \sqrt{1 + p^{2}}}{\sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}\),
and
\(P=\frac{p \sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}{\sqrt{1 + p^{2}}}\),
the minimal trajectory must satisfy \(M dy = dP\), i.e.,

\[\frac{-F_{x} dy \sqrt{1 + p^{2}}}{\sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}} = 
\frac{dp \sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}{\left( 1 + p^{2} \right) \sqrt{1 + p^{2}}} - 
\frac{p F_{y} dy - p F_{x} dx}{\sqrt{1 + p^{2}}\sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}\]

which may be written as

\[\frac{dp \sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}{\left( 1 + p^{2} \right) \sqrt{1 + p^{2}}} = 
\frac{F_{y} dx - F_{x} dy}{\sqrt{1 + p^{2}}\sqrt{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}}\]

or, more simply,

\[\frac{dp}{1 + p^{2}} = 
\frac{F_{y} dx - F_{x} dy}{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}\]

This trajectory agrees with experience, as shown by replacing
\(v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy\) with \(v^{2}\),

\[\frac{v^{2} dp}{\left( 1 + p^{2} \right)^{3/2}} = 
\frac{F_{y} dx - F_{x} dy}{\sqrt{1 + p^{2}}}\]

The instantaneous radius of curvature of the trajectory is defined
\(R \equiv - \frac{\left( 1 + p^{2} \right)^{3/2} dy}{dp}\), yielding
the equation \(\frac{v^{2}}{R} = \frac{F_{x} dy - F_{y} dx}{ds}\). Here,
\(\frac{v^{2}}{R}\) is the centripetal acceleration required to produce
the trajectory's curvature, whereas \(\frac{F_{x} dy - F_{y} dx}{ds}\)
is the force (perpendicular to the trajectory) that provides that
centripetal acceleration. Their equality is evidence that our method
agrees with experience.

\textbf{8.} The resulting equation
\(\frac{dp}{1 + p^{2}} = \frac{F_{y} dx - F_{x} dy}{v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy}\)
is generally integrable. Multiplication by
\(\frac{p \left( v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy \right)}{1 + p^{2}}\)
yields the equation

\[\frac{p dp \left( v_{0}^{2} - \int 2 F_{x} dx - \int 2 F_{y} dy \right)}{\left( 1 + p^{2} \right)^{2}} + \frac{F_{x} dx - p^{2} F_{y} dy}{1 + p^{2}} = 0\]

which may be integrated as

\[\frac{\int 2 F_{x} dx - p^{2} \int 2 F_{y} dy - v_{0}^{2}}{1 + p^{2}} = C\]

or, more simply,

\[\int 2 F_{x} dx - p^{2} \int 2 F_{y} dy = v_{0}^{2} + C + C p^{2}\]

Hence, \(p = \sqrt{\frac{B + \int 2 F_{x} dx}{C + \int 2 F_{y} dy}}\),
if we define \(B \equiv -v_{0}^{2} - C\). Then, since
\(p = \frac{dx}{dy}\), we may separate the variables \(x\) and \(y\)

\[\int \frac{dx}{\sqrt{B + \int 2 F_{x} dx }} = \int \frac{dy}{\sqrt{C + \int 2 F_{y} dy }}\]

in the equation for the desired trajectory. Converting the constants
\(B\) and \(C\) into their negatives, we obtain

\[\int \frac{dx}{\sqrt{B - \int 2 F_{x} dx }} = \int \frac{dy}{\sqrt{v_{0}^{2} - \int 2 F_{y} dy }}\]

Although the construction of the trajectory is relatively easy in
principle, it may be difficult to express that trajectory in algebraic
equations. For example, let \(F_{x}\) and \(F_{y}\) be proportional to
the same power \(n\) of \(x\) and \(y\), respectively

\[\int \frac{dx}{\sqrt{b^{n} - x^{n}}} = \int \frac{dy}{\sqrt{a^{n} - y^{n}}}\]

If \(n=1\), this equation describes a parabola whereas, if \(n=2\), it
describes an ellipse centered on C. (Even in this case, each integration
requires trigonometric quadratures.) Similarly, in other cases, neither
integration succeeded in producing trajectories described by algebraic
functions. Nevertheless, this method for finding these trajectories is
still desirable.

\textbf{9.} Now let the particle at M be acted upon by a central force,
i.e., one that always lies along the direction MC and is a function
\(F_{r}(r)\) only of the distance MC = \(r\) (Figure 27). As before, let
CP = \(y\), PM = \(x\), and \(dx = p dy\); thus, CM =
\(\sqrt{x^{2} + y^{2}} = r\). Let \(F_{r}\) be resolved into
perpendicular components along the lines MQ and MP; the force along MQ
is \(\frac{F_{r} y}{r}\), whereas the force along MP equals
\(\frac{F_{r} x}{r}\). These forces give rise to an acceleration
\(dv^{2} = - \frac{2 F_{r} x dx}{r} - \frac{2 F_{r} y dy}{r} = - 2 F_{r} dr\)
(because \(x dx + y dy = r dr\)), from which we have
\(v^{2} = v_{0}^{2} - \int 2 F_{r} \, dr\). Thus, the expression to be
minimized is the integral
\(\int dy \sqrt{1 + p^{2}} \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}\).
Following our protocol, we must differentiate the integrand
\(\sqrt{1 + p^{2}} \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}\), yielding

\[-\frac{F_{r} dr \sqrt{1 + p^{2}}}{\sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}} + \frac{p dp \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}{\sqrt{1 + p^{2}}}\]

Since \(dr = \frac{x dx + y dy}{r}\), we may assign
\(M = \frac{- F_{r} x \sqrt{1 + p^{2}}}{r \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}\)
and
\(P = \frac{p \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}{\sqrt{1 + p^{2}}}\);
using the general law for optimal curves \(M dy = dP\), we obtain

\[\frac{- F_{r} x dy \sqrt{1 + p^{2}}}{r \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}} = 
\frac{dp \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}{\left( 1 + p^{2} \right) \sqrt{1 + p^{2}}} - 
\frac{p F_{r} dr}{\sqrt{1 + p^{2}} \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}\]

which may be simplified to

\[\frac{F_{r} \left( y dx - x dy\right)}{r \left( v_{0}^{2} - \int 2 F_{r} \, dr \right)} = \frac{dp}{1 + p^{2}}\]

\textbf{10.} Although this equation contains four separate variables
(\(x, y, p\) and \(r\)), it can be integrated (with sufficient skill).
For we know that \(x dx + y dy = r dr = p x dy + y dy\); thus,
\(dy = \frac{r dr}{y + px}\) and \(dx = \frac{p r dr}{y + px}\).
Substituting these values into the equation yields

\[\frac{\left( p y - x\right) F_{r} dr}{\left( y + p x \right)  \left( v_{0}^{2} - \int 2 F_{r} \, dr \right)} = \frac{dp}{1 + p^{2}}\]

which may be written as

\[\frac{F_{r} dr}{v_{0}^{2} - \int 2 F_{r} \, dr} = \frac{dp \left( p x + y \right)}{\left( 1 + p^{2} \right) \left( p y - x\right)}\]

Each of these two terms can be integrated using logarithms.
Specifically,
\(\int \frac{F_{r} dr}{v_{0}^{2} - \int 2 F_{r} \, dr } = - \frac{1}{2} \log \left( v_{0}^{2} - \int 2 F_{r} \, dr \right)\);
expanding in partial fractions,
\(\int \frac{dp \left( p x + y \right)}{\left( 1 + p^{2} \right) \left( p y - x\right)}\)
resolves into
\(\int \frac{y dp}{p y - x} - \int \frac{p dp}{1 + p^{2}} = \log \frac{p y - x}{\sqrt{1 + p^{2}}}\).
Thus, we have
\(\frac{C}{\sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}} = \frac{p y - x}{\sqrt{1 + p^{2}}}\).
This equation states that the particle speed
\(v = \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}\) is inversely proportional
to the component of the radius that is perpendicular to the trajectory,
a handy property of these motions.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.25\textwidth]{Fig/Euler_Methodus_inveniendi_Additamentum_II_Fig28.png}
\caption*{Figure 28}
\end{figure}

\textbf{11.} This same problem may be solved more conveniently by
choosing different variables (Figure 28). The method described above
does not require that the coordinates be orthogonal to each other,
merely that they suffice to define the trajectory uniquely. For this
reason, we cannot take as our two variables the radius CM and its
component perpendicular to the trajectory, since some trajectories
cannot be described using these variables. However, we \emph{can} take
as our coordinates the radius CM and the arc BP of a unit circle
centered on C; these two coordinates suffice to determine every point
exactly, just as for orthogonal coordinates. This ability to change
variables extends the applicability of our Method more broadly than
might be imagined otherwise.

\textbf{12.} Let the radius of the particle be CM = \(r\), and let the
central force \(F_{r}(r)\) be some function of the radius \(r\). We take
as our second variable \(\theta\), the arc-length BP of a unit circle
centered on C; the infinitesimal arc \(d\theta\) corresponds to Pp in
Figure 28. Defining \(p \equiv \frac{d\theta}{dr}\), the applied force
results in an acceleration \(dv^{2} = - 2 F_{r} \, dr\), from which we
obtain \(v^{2} = v_{0}^{2} - \int 2 F_{r} \, dr\). The trajectory sweeps
out an infinitesimal circular arc Mn of radius CM = \(r\) about the
center C, whereas mn represents the infinitesimal change in radius
\(dr\). From the geometrical similarity CP:Pp = CM:Mn follows the
equation Mn = \(p r dr\), and the infinitesimal distance Mm =
\(ds = dr \sqrt{1 + p^{2} r^{2}}\). Thus, we seek the trajectory that
minimizes the integral
\(\int dr \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr} \sqrt{1 + p^{2} r^{2}}\).
From this, we may obtain the differential
\(\frac{d}{dr} \left[ \frac{p r^{2} \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}{\sqrt{1 + p^{2} r^{2}}} \right]\),
which equals zero, following our general protocol. This yields the
equation
\(\sqrt{C} = \frac{p r^{2} \sqrt{v_{0}^{2} - \int 2 F_{r} \, dr}}{\sqrt{1 + p^{2} r^{2}}}\),
or rather
\(C + C p^{2} r^{2} = \left( v_{0}^{2} - \int 2 F_{r} \, dr \right) p^{2} r^{4}\),
from which follows

\[p = \frac{\sqrt{C}}{\sqrt{\left( v_{0}^{2} - \int 2 F_{r} \, dr \right) r^{4} - C r^{2}}} = \frac{\sqrt{C}}{r \sqrt{\left( v_{0}^{2} - \int 2 F_{r} \, dr \right) r^{2} - C }}\]

which may be written as

\[d\theta = \frac{dr \sqrt{C}}{r \sqrt{\left( v_{0}^{2} - \int 2 F_{r} \, dr \right) r^{2} - C }}\]

This same equation may also be derived using direct methods of
mechanics.

\textbf{13.} In these examples, our principle agrees perfectly with
experience; and the suspicion that it will fail in more complicated
cases can likely be overcome. However, our principle should be studied
carefully to determine the limits of its validity. To explain what I
mean, all motion of particles can be divided into two classes, one in
which position alone determines the speed and another in which it does
not. In the first case, the particle always has the same speed when it
reaches the same position; for example, the speed of a particle acted
upon by one or more centers of force will be a function only of the
distances to those center(s). In the second case, the speed is not
determined solely by the position of the particle; for example, the
centers of force themselves may be moving or the particle may be in a
resistant medium. \emph{Our principle pertains only to the first class
of motion}; in particular, even when a particle is attracted to multiple
fixed centers of force, its momentum integrated along its trajectory
should be minimal.

\textbf{14.} Here is the essence of our hypothesis. We seek the
trajectory between two specified endpoints that minimizes the integral
\(\int ds \, v\) where, by assumption, the speed \(v\) of the particle
at the endpoints does not depend on the path travelled. Even if there
are many fixed centers of force, the speed \(v\) can be expressed as a
well-defined function of the variables CP = \(y\) and PM = \(x\) (Figure
27). Thus, the squared speed \(v^{2}\) is also a function of \(x\) and
\(y\), and we may write \(dv^{2} = 2 F_{x} dx + 2 F_{y} dy\), from which
we may see that our principle agrees with the true trajectory of the
particle. Since \(dv^{2} = 2 F_{x} dx + 2 F_{y} dy\), the forces acting
on the particle at a point M can be grouped into two forces, a force
\(F_{x}\) that acts along the \(x\) direction and another force
\(F_{y}\) that acts in the \(y\) direction. These forces can themselves
be resolved into a tangential force along the trajectory
\(\frac{F_{x} dx + F_{y} dy}{ds}\), and a normal force perpendicular to
the trajectory \(\frac{-F_{y} dx + F_{x} dy}{ds}\). The normal force
provides the local centripetal acceleration
\(\frac{v^{2}}{R} = \frac{-F_{y} dx + F_{x} dy}{ds} = \frac{F_{x} - p F_{y}}{\sqrt{1 + p^{2}}}\),
where \(R\) is the local radius of curvature of the trajectory. Thus,
our principle of deriving mechanics from a principle of maxima and
minima conforms with experience.

\textbf{15.} We seek the trajectory that minimizes the integral
\(\int dy \, v \sqrt{1 + p^{2}}\); therefore, by our established
protocol, we must differentiate the quantity \(v \sqrt{1 + p^{2}}\).
Since \(dv^{2} = 2 F_{x} dx + 2 F_{y} dy\), the differentiation yields

\[\frac{F_{x} dx \sqrt{1 + p^{2}}}{v} + 
\frac{F_{y} dy \sqrt{1 + p^{2}}}{v} +
\frac{p dp v}{\sqrt{1 + p^{2}}}\]

From our general law \(Mdy = dP\), we obtain the desired trajectory

\[\frac{F_{x} dy \sqrt{1 + p^{2}}}{v} = 
d\left[\frac{p v}{\sqrt{1 + p^{2}}} \right] = 
\frac{dp v}{\left( 1 + p^{2} \right)^{3/2}} + 
\frac{p F_{x} dx + p F_{y} dy}{v \sqrt{1 + p^{2}}}\]

which may be written as

\[\frac{- dp v}{\left( 1 + p^{2} \right)^{3/2}} =
\frac{F_{y} p dy - F_{x} dy}{v \sqrt{1 + p^{2}}}\]

The instantaneous radius of curvature of the trajectory at M equals
\(R = \frac{- \left( 1 + p^{2} \right) dy \sqrt{1 + p^{2}}}{dp}\), from
which follows
\(\frac{v^{2}}{R} = \frac{F_{y} p - F_{x}}{\sqrt{1 + p^{2}}}\), just as
predicted from the direct methods of mechanics. Thus, provided that the
external forces can be resolved along the coordinate directions into two
forces \(F_{x}\) and \(F_{y}\) that are functions of these variables
\(x\) and \(y\), then every trajectory will represent the minimum of the
integrated momentum over the path.

\textbf{16.} Therefore, this principle is broadly applicable, except to
the case of motion in a resistant medium. The reason for this exception
is easy to see, because the speed of the particle at the endpoints will
depend on the path taken. Hence, neglecting any resistance to the
particle's motion, the momentum integrated along the path should be a
minimum. Moreover, this minimum law is true not only for the motion of
single particles, but also for systems of particles bound together. No
matter what their reciprocal interactions are, the path integral of
their momenta is always minimal. Compared to traditional mechanics
methods, the motion may be more difficult to calculate using our new
method; however, it seems easier to grasp from first principles. Because
of their inertia, bodies are reluctant to move, and obey applied forces
as though unwillingly; hence, external forces generate the smallest
possible motion consistent with the endpoints. A rigorous proof for this
principle is lacking, I realize. Nevertheless, it agrees with experiment
and I do not doubt that it will be verified by stronger proofs that use
the principles of a complete Metaphysics. But such proofs I leave to the
professors of Metaphysics.

\textbf{17.}
To conclude, I remind this formula
\begin{equation}
e^{i\pi} + 1 = 0
\end{equation}

\subsection{External link}\label{external-link}

\begin{itemize}
\tightlist
\item
  \href{http://math.dartmouth.edu/~euler/docs/originals/E065h}{PDF of
  original Additamentum II (Appendix 2)}
\end{itemize}

\href{la:Methodus_inveniendi/Additamentum_II}{la:Methodus
inveniendi/Additamentum II}

\url{Category:Physics} \url{Category:Mathematics}
\href{Category:History_of_science}{Category:History of science}
\href{Category:Early_modern_works}{Category:Early modern works}
\href{Category:Works_originally_in_Latin}{Category:Works originally in
Latin} \url{Category:Switzerland}

\subsection{Note from the contributor}

Hi, I undertook this translation of Euler's appendix because I needed it
for a planned series of articles about the various types of the ``action
principle'' in physics and this history of variational principles in
physics. It was fun, especially seeing how the science of mechanics
developed.

The manuscript from which I worked may be found
\href{http://math.dartmouth.edu/~euler/docs/originals/E065h}{here}. The
original has also been added to the \href{:la:Methodus_inveniendi}{Latin
Wikisource}.

This translation of Euler's \emph{Methodus inveniendi -\/- Additamentum
II} is hereby released into the public domain. Use it well.
\href{User:WillowW}{WillowW} 11:37, 25 June 2006 (UTC)

\textbf{Historical notes}

This appendix describes Euler's independent invention of
\textbf{Maupertuis' principle}, which states that the true trajectory
followed by a mechanical system between two specified endpoints is an
extremum of the integral of momentum over distance travelled,
\(\int p dq\) in modern notation. Maupertuis' principle is \emph{not}
the same as \textbf{Hamilton's principle}; the former considers only the
path through \emph{space} and requires the conservation of energy,
whereas the latter considers the path through \emph{space and time} and
has no special requirements. Euler lays the groundwork here for
Hamilton's principle by noting that the integral of momentum over
distance could be converted into an integral of kinetic energy over time
(paragraph 2); however, his main purpose in doing so was to make his
minimum principle acceptable to the followers of both schools of
mechanics (that of Newton and that of Leibniz), who were having a bitter
conflict long after both physicists were dead.

Maupertuis and Euler conceived this ``principle of least action''
independently of one another, but Maupertuis has priority, since he
submitted it for publication a few months prior to Euler (1744). Euler
himself gave Maupertuis credit for this principle in his later articles
and defended his priority vigourously when it was challenged in 1751 by
Samuel König. However, neither Maupertuis nor Euler got the principle
quite right; neither recognized the requirement of energy conservation
explicitly, nor the possibility of other types of extrema (maxima or
inflection) of the integral. In contrast to Maupertuis, however, Euler
recognized that his principle pertains only when the speed is a function
of position alone and presents it here as a hypothesis worthy of further
investigation.

In paragraph 11, Euler lays the groundwork for Lagrange's
\emph{generalized coordinates} by noting that his principle can be
expressed in different coordinates and that this is a useful trick for
solving a wider variety of problems than can be solved merely in
Cartesian coordinates. Euler illustrates this by solving the ``central
force'' problem in polar coordinates. In paragraph 16, Euler also states
without proof that the principle can be applied to \emph{systems} of
particles, rather than merely to a single particle (as done in the
remainder of the article and also by Maupertuis).

\textbf{Translation status:} Complete, but minor touchups might still be
needed. Three obscure readings could use some clarification from a
better Latin/physics scholar than me; see the translation notes below.
Thanks for your help! :D

\textbf{Notes on the Translation}

I have a few (minor) changes in Euler's presentation to make the article
more intuitive for modern readers:

\begin{itemize}
\tightlist
\item
  Consistent with modern custom, I used \(v\) and \(v^{2}\) for the
  speed and squared speed, respectively. In the original article, Euler
  uses \(\sqrt{v}\) and \(v\) for the same quantities, although he uses
  \(v\) and \(v^{2}\) in other articles. Euler's \(v\) might stand for
  \emph{vis viva} (kinetic energy). As an aside, ``speed/velocity'' in
  Latin (\emph{celeritas}) starts with a ``c'', not a ``v''.
\end{itemize}

\begin{itemize}
\tightlist
\item
  Consistent with modern custom, I took \(x\) and \(y\) as the
  horizontal and vertical coordinates, respectively. In the original
  article, Euler chose the reverse.
\end{itemize}

\begin{itemize}
\tightlist
\item
  I used the variable \(t\) \emph{only} for time, the variable \(r\)
  \emph{only} for the radius to the origin of the coordinate system and
  \(R\) for the instantaneous radius of curvature of the trajectory. In
  the original article, Euler used \(t\) for the first two variables
  (albeit in well-distinguished settings) and \(r\) for the last
  variable.
\end{itemize}

\begin{itemize}
\tightlist
\item
  When Euler says ``force'' (\emph{vis}), he seems to mean a quantity
  that is \emph{half} of our modern use of the word ``force''.
  Therefore, I have introduced a factor of two into the force equations.
\end{itemize}

\begin{itemize}
\tightlist
\item
  I have indicated force components by an \emph{F} with a subscript,
  e.g., the radial force \(F_{r}\). In the original article, Euler uses
  various capital letters (\(X, Y, T\) and \(V\)) which may be confused
  with constants of integration. It should be remembered that Euler was
  writing long before the development of vectors.
\end{itemize}

\begin{itemize}
\tightlist
\item
  Consistent with their notation and usage in the article, I translated
  \emph{spatiolus \(ds\)}, \emph{tempusculus \(dt\)} and \emph{arculus}
  as ``infinitesimal distance \(ds\)'', ``infinitesimal time \(dt\)''
  and ``infinitesimal arc''.
\end{itemize}

\begin{itemize}
\tightlist
\item
  I translated \emph{corpus projectum} as ``particle'' or ``moving
  particle'', since Euler clearly is discussing the trajectories of
  masses moving under the influence of generic external forces, and not
  merely the flight of cannonballs in a uniform gravitational field.
  Moreover, the rotational degrees of freedom of the mass are not
  considered.
\end{itemize}

\begin{itemize}
\tightlist
\item
  I did not include a translation of the phrase \emph{debita altitudine}
  (``resulting from its height'') in the first line of paragraph 2. Read
  in the context of the whole article, Euler is likely trying to specify
  that the velocity must be a function only of position, but the phrase
  seems unnecessary and distracts from the main exposition.
\end{itemize}

\begin{itemize}
\tightlist
\item
  In paragraph 12, Euler does not use a unit circle, but rather one of
  generic radius \(c\). To simplify the presentation, I have set
  \(c=1\), with no loss of generality.
\end{itemize}

\begin{itemize}
\tightlist
\item
  The last sentence of paragraph 5 is a little obscure to me; I think
  I've translated it OK, but it may need correcting. Suggestions are
  welcome!
\end{itemize}

\begin{itemize}
\tightlist
\item
  The conversion of B and C into their negatives in paragraph 8 is also
  obscure to me. Can anyone help me out? :)
\end{itemize}

\begin{itemize}
\tightlist
\item
  The Latin sentence beginning ``Because of their inertia'' near the end
  of paragraph 16 is also obscure to me. Suggestions here are also
  welcome!
\end{itemize}

In general, I tried to ``breathe life'' into the translation, i.e., not
translate it word-by-word, but capture Euler's \emph{meaning} in modern
English. It wasn't hard, since Euler is quite modern in his thinking,
albeit with a few twists. I like his use of the instantaneous radius of
curvature of the trajectory, which is rarely seen in physics textbooks,
being found mainly in math books (differential geometry). I also like
his liberal usage of the Bernoulli \(p\) variable and, more generally,
implicit differentiation.

\end{document}
