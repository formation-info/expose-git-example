#!/bin/bash

cd ..
case $1 in
	0)
	cp .modif/methodus-inveniendi-addII_0_submitted.tex methodus-inveniendi-addII.tex
	;;
	1)
	cp .modif/methodus-inveniendi-addII_1_bernoulli_1_number.tex methodus-inveniendi-addII.tex
	;;
	2)
	cp .modif/methodus-inveniendi-addII_2_euler_1_formula.tex methodus-inveniendi-addII.tex
	;;
	3)
	cp .modif/methodus-inveniendi-addII_3_bernoulli_2_title.tex methodus-inveniendi-addII.tex
	;;
	4)
	cp .modif/methodus-inveniendi-addII_3_euler_2_title.tex methodus-inveniendi-addII.tex
	;;
	5)
	cp .modif/methodus-inveniendi-addII_4_merge.tex methodus-inveniendi-addII.tex
esac
